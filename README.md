# SC Demo Server

Dynamický demo backend pre SC v NodeJS.

Slúži len na spracovaníe vyhľadávania zasielaného metódou `POST`.

## Install

```
git clone https://gitlab.com:sc-open/sc-demo-server.git
cd sc-demo-server
yarn
yarn start
```

## Knižnica na generovanie menu

V adresári `/build` je skompilovaný TypeScript kód veľmi jednoduchej knižnice na vytváranie menu stránok
kompatbilných s protokolom SC. Vývoj takejto knižnice je predmetom ďalšieho výskumu a preto nie je priložený
pôvodný kód.

Ukážka ako vygenerovať jednoduché menu je v súbore `src/menu2json.js`.

## Deployment

Projekt je postavený na knižnici `fastify` a bol úspešne odskúšaný cez `vercel.com`.

## Príklady vyhľadávania

### /Search/ws

Po zaslaní ľubovolného identu získaného vyhľadávaním na WS sa vygenerujú potrebné stránky, ktoré síce nemajú žiadnu
znalosť o streame, ale dovedú užívateľa k prehrávaniu.

### /Search/csfd

Súbor `src-private/csfd-map.json` nie je commitnutý a mal by obsahovať vlastnú mapu ČSFD identifikátorov 
proti názvu a internému identifikátoru v statickej databáze.

```json
{
  "2":{"id":"2155","title":"Připoutejte se, prosím! - CZ, EN, SK (1980)"}
}
```  

### /Search/add

Prázdna funkcia, ktorá prijme komentár od užívateľa a je možné ju rozšíriť pre príjem odkazov na nové streamy. 
