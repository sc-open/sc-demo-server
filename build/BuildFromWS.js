"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BuildFromWS = void 0;
const Video_1 = require("./items/Video");
const Stream_1 = require("./items/Stream");
class BuildFromWS {
    static fromWsIdent(ident) {
        return Video_1.createVideo("/ws-ident/" + ident, "WS ident = " + ident);
    }
    static streamWsIdent(ident, list = false) {
        const stream = Stream_1.createStream('/ws-stream/' + ident, 'WS stream = ' + ident, 'webshare', ident);
        if (list) {
            return {
                "strms": [
                    stream
                ],
                "info": Video_1.createVideo('/ws-ident/' + ident, 'WS ident = ' + ident),
                "system": null
            };
        }
        else {
            return stream;
        }
    }
}
exports.BuildFromWS = BuildFromWS;
