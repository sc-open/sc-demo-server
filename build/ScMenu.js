"use strict";
var __classPrivateFieldGet = (this && this.__classPrivateFieldGet) || function (receiver, privateMap) {
    if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to get private field on non-instance");
    }
    return privateMap.get(receiver);
};
var __classPrivateFieldSet = (this && this.__classPrivateFieldSet) || function (receiver, privateMap, value) {
    if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to set private field on non-instance");
    }
    privateMap.set(receiver, value);
    return value;
};
var _system;
Object.defineProperty(exports, "__esModule", { value: true });
exports.ScMenu = void 0;
const BuildFromWS_1 = require("./BuildFromWS");
var Build_1 = require("./Build");
Object.defineProperty(exports, "Build", { enumerable: true, get: function () { return Build_1.Build; } });
Object.defineProperty(exports, "CmdType", { enumerable: true, get: function () { return Build_1.CmdType; } });
class Movie {
}
class Serie {
}
class Episode {
}
class Stream {
}
class ScMenu {
    constructor() {
        this.items = [];
        _system.set(this, null);
    }
    add(item) {
        (Array.isArray(item) ? item : [item]).filter(item => !!item).forEach(item => this.items.push(item));
        return this;
    }
    static fromWsIdent(ident) {
        const menu = new ScMenu();
        menu
            .add(BuildFromWS_1.BuildFromWS.fromWsIdent(ident))
            .setContent('movies');
        return menu;
    }
    setContent(content) {
        __classPrivateFieldSet(this, _system, Object.assign(__classPrivateFieldGet(this, _system) || {}, { setContent: content }));
    }
    toJSON() {
        const r = {
            menu: this.items
        };
        if (__classPrivateFieldGet(this, _system)) {
            r.filter = {
                "pg": true,
                "view": "movies",
                "limit": 500,
                "page": "2",
                "typ": 1,
                "l_cs": "t",
                "of": "name_cs_seo",
                "od": "asc",
                "meta": {
                    "total": "1170",
                    "total_found": "1170",
                    "time": "0.043"
                }
            };
            r.system = {
                "addSortMethods": [
                    39,
                    19,
                    30,
                    26,
                    18,
                    21,
                    21
                ],
                "setContent": "movies"
            };
        }
        return r;
    }
}
exports.ScMenu = ScMenu;
_system = new WeakMap();
