"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createStream = void 0;
function createStream(url, title, provider, ident) {
    return {
        mediatype: 'movie',
        url,
        title,
        provider,
        ident
    };
}
exports.createStream = createStream;
