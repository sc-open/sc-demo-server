"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createVideo = void 0;
function createVideo(url, title) {
    return {
        type: 'video',
        poster: '',
        img: 'icons/movie.png',
        mediatype: 'movie',
        url,
        title
    };
}
exports.createVideo = createVideo;
