"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Build = exports.CmdType = void 0;
var CmdType;
(function (CmdType) {
    CmdType[CmdType["PluginSettings"] = 1] = "PluginSettings";
    CmdType[CmdType["UpdateAddonsRepos"] = 2] = "UpdateAddonsRepos";
})(CmdType = exports.CmdType || (exports.CmdType = {}));
class Build {
    static text(text, img = 'DefaultIconInfo.png') {
        return text.split('\n').map(t => {
            return {
                type: 'dir',
                title: t,
                img
            };
        });
    }
    static link(title, url, img = 'icons/submenu/DefaultFolder.png') {
        return {
            type: 'dir',
            title,
            url,
            img
        };
    }
    static trakt() {
        return {
            "type": "dir",
            "title": "$30943 (trakt.tv)",
            "action": "traktWatchlist",
            "img": "defaultplaylist.png",
            "visible": "String.IsEqual(Window(Home).Property(sc.trakt), True)"
        };
    }
    static cmd(cmd) {
        switch (cmd) {
            case CmdType.PluginSettings:
                return {
                    type: 'dir',
                    title: '[B]$30922[/B]',
                    url: 'cmd://Addon.OpenSettings("plugin.video.stream-cinema")',
                    poster: '',
                    img: 'icons/settings.png'
                };
        }
    }
    static search(title = '$30902', id = 'all', img = 'icons/search.png') {
        return {
            type: "dir",
            title,
            id,
            action: "csearch",
            img
        };
    }
}
exports.Build = Build;
