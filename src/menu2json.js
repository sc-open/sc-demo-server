/**
 * Ukazka ako si rychlo vygenerovat funkcnu stranku menu
 *
 * Obsah adresara /build je zgenerovany z TypeScript kodu, ktory bol len narychlo vytvoreny,
 * preto tu nie je jeho zdrojovy kod. Zmysel ma vytvorit novy projekt, ktory bude napomocny
 * pri generovani validnych resposov.
 */

const { ScMenu, Build, CmdType } = require('../build/ScMenu')

console.log(JSON.stringify(new ScMenu()
  .add(Build.text(`používaš demo databázu kompatibilnú s protokolom SC1
jedná sa o experiment a predmet výskumu, nie dostatočne kvalitný zdroj dát

zatiaľ nie je nič zdokumentované  
[COLOR red]prídi neskôr a snáď niečo dopíšem[/COLOR]`))
  .add(Build.link('[B]Na hlavné menu[/B]', '/', 'icons/menu.png'))
  .toJSON()))
