const path = require('path')

const { BuildFromWS } = require('../build/BuildFromWS')
const { createVideo } = require('../build/items/Video')
const { ScMenu, Build, CmdType } = require('../build/ScMenu')
const csfdMap = require('../src-private/csfd-map.json')

const Fastify = require('fastify')

function build () {
  const fastify = Fastify({ logger: true })
  fastify.register(require('fastify-formbody'))

  /**
   *
   */
  fastify.register(require('fastify-static'), {
    root: path.join(__dirname, '../public'), // sc2sc
    serve: false
  })

  class BuildFromDB {
    fromCsfdId (id) {
      const vs = []

      if (Object.prototype.hasOwnProperty.call(csfdMap, id)) {
        vs.push(createVideo('/Play/' + csfdMap[id].id, csfdMap[id].title))
      }

      return vs
    }
  }

  const db = new BuildFromDB()

  /**
   * Handle main page request
   *
   * This shows possible builder functions.
   * If the page is static,
   * the fastest way is to use sendFile of prepared content located in public:
   *
   * `reply.sendFile('index')`
   */
  fastify.get('/kodi/', async (request, reply) => {
    const menu =
      new ScMenu()
        .add(Build.trakt())
        .add(Build.search('Spustenie cez WS ident', 'ws', 'DefaultFile.png'))
        .add(Build.search('Spustenie cez ČSFD', 'csfd', 'DefaultVideoPlaylists.png'))
        .add(Build.search('Pridaj stream (pozri návod)', 'add', 'DefaultAddSource.png'))
        .add(Build.link('Filmy na WS podľa SC', '/FMovies', 'DefaultMovies.png'))
        .add(Build.link('[COLOR yellow]Návod[/COLOR]', '/help', 'DefaultAddonHelper.png'))
        .add(Build.cmd(CmdType.PluginSettings))
        .add(Build.text('používaš experimentálny zdroj dát!!! [COLOR blue]sťažnosti@lamparen.*[/COLOR]', 'DefaultIconWarning.png'))
        .toJSON()

    return reply.send(menu)
  })

  /**
   * Return some text rows as help.
   * Also good candidate to be served as prepared file.
   */
  fastify.get('/kodi/help', async (request, reply) => {
    return reply.send(new ScMenu()
      .add(Build.text(`tieto riadky
sa zobrazia po riadkoch ako informacie 
[COLOR red]a daju sa pouzit aj dekoracie[/COLOR]`))
      .toJSON()
    )
  })

  /**
   * Handle play by WS ident
   */
  fastify.post('/kodi/Search/ws', async (request, reply) => {
    return reply.send(
      ScMenu
        .fromWsIdent(request.body.search)
        .toJSON()
    )
  })

  /**
   * Handle play by WS ident
   */
  fastify.post('/kodi/Search/add', async (request, reply) => {
    request.log.warn('ADD: ' + request.body.search)
    return reply.send(
      new ScMenu()
        .add(Build.text('uvidíme, čo s tým'))
        .toJSON()
    )
  })

  /**
   * Handle play by CSFD id
   */
  fastify.post('/kodi/Search/csfd', async (request, reply) => {
    try {
      const menu = new ScMenu()
      const found = db.fromCsfdId(request.body.search)
      if (found.length > 0) {
        menu
          .add(found)
          .setContent('movies')
      } else {
        menu
          .add(Build.search('Pridaj stream (pozri návod)', 'add', 'DefaultAddSource.png'))
          .add(Build.text('[COLOR magenta]aktuálne nie je známy stream pre takúto položku[/COLOR]'))
          .add(Build.link('[COLOR yellow]Návod[/COLOR]', '/help', 'DefaultAddonHelper.png'))
          .add(Build.link('[B]Hlavné menu[/B]', '/', 'icons/menu.png'))
      }

      return reply.send(menu.toJSON())
    } catch (ex) {
      console.error(ex.message)
      throw ex
    }
  })

  /**
   * Return stream for WS ident
   */
  fastify.get('/kodi/ws-stream/:ident', async (request, reply) => {
    return reply.send(
      BuildFromWS
        .streamWsIdent(request.params.ident)
    )
  })

  /**
   * Return stream for WS ident
   */
  fastify.get('/kodi/ws-ident/:ident', async (request, reply) => {
    return reply.send(
      BuildFromWS
        .streamWsIdent(request.params.ident, true)
    )
  })

  /**
   * Fallback any other GET request to static content in public folder
   */
  fastify.get('*', (request, reply) => {
    return reply.sendFile(request.params['*'].substr(0))
  })

  return fastify
}

module.exports = build
